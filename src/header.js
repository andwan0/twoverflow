/*!
 * __overflow_title v__overflow_version
 */

;(function (window, undefined) {

var rootScope = injector.get('$rootScope')
var $rootScope = rootScope
var transferredSharedDataService = injector.get('transferredSharedDataService')
var modelDataService = injector.get('modelDataService')
var socketService = injector.get('socketService')
var routeProvider = injector.get('routeProvider')
var eventTypeProvider = injector.get('eventTypeProvider')
var windowDisplayService = injector.get('windowDisplayService')
var windowManagerService = injector.get('windowManagerService')
var angularHotkeys = injector.get('hotkeys')
var armyService = injector.get('armyService')
var villageService = injector.get('villageService')
var mapService = injector.get('mapService')
var $filter = injector.get('$filter')
var storageService = injector.get('storageService')
